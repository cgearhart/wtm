
import hashlib
import os
import uuid

from datetime import datetime, date
from json import dumps, loads

from flask import Flask, request, make_response, render_template, session
from flask import flash, url_for, redirect
from flask.ext.sqlalchemy import SQLAlchemy

from sqlalchemy.orm import class_mapper
from sqlalchemy.exc import IntegrityError


app = Flask(__name__)

app.secret_key = os.environ['SECRET_KEY']
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ['DATABASE_URL']

# app.debug = True
# app.config['SQLALCHEMY_DATABASE_URI'] = 'postgres://localhost/wtm'
# app.secret_key = 'abc'

db = SQLAlchemy(app)


class Users(db.Model):
    _id = db.Column(db.Text,
                    default=lambda: str(uuid.uuid4()),
                    primary_key=True)
    name = db.Column(db.Text, unique=True)
    modified = db.Column(db.DateTime,
                         default=db.func.now(),
                         onupdate=db.func.now())
    pwd = db.Column(db.Text)

    def __init__(self, name, pwd, modified=None, _id=None, **kwargs):
        self.name = name
        self.pwd = pwd
        if modified:
            self.modified = modified
        if _id:
            self._id = _id


class Tasks(db.Model):
    _id = db.Column(db.Text,
                    default=lambda: str(uuid.uuid4()),
                    primary_key=True)
    name = db.Column(db.Text)
    note = db.Column(db.Text)
    checked = db.Column(db.Boolean, default=False)
    noduetime = db.Column(db.Boolean, default=True)
    duetime = db.Column(db.Date)
    priority = db.Column(db.Integer, default=0)
    deleted = db.Column(db.Boolean, default=False)
    modified = db.Column(db.DateTime,
                         default=db.func.now(),
                         onupdate=db.func.now())
    userid = db.Column(db.Text, db.ForeignKey('users._id'))
    user = db.relationship('Users',
                           backref=db.backref('tasks', lazy='dynamic'))

    def __init__(self, name, note, userid,
                 checked=None, noduetime=None, duetime=None,
                 priority=None, modified=None, _id=None, **kwargs):
        self.name = name
        self.note = note
        self.userid = userid
        if checked:
            self.checked = checked
        if noduetime:
            self.noduetime = noduetime
        if duetime:
            self.duetime = duetime
        if priority:
            self.priority = priority
        if modified:
            self.modified = modified
        if _id:
            self._id = _id


def serialize(model, skip_columns=None):
    """Transforms a model into a dictionary which can be dumped to JSON."""
    if skip_columns is None:
        skip_columns = []
    columns = [c.key for c in class_mapper(model.__class__).columns
               if c.key not in skip_columns]
    return dict((c, str(getattr(model, c))) for c in columns)


@app.route('/users')
def getUsers():
    """
    Get all user records from the database that are newer than the 'modified'
    parameter (or unix epoch if omitted), and return them in a json-encoded
    list
    """
    date = request.args.get('modified',
                            datetime.utcfromtimestamp(0).isoformat(' '))
    serialized_labels = [
        serialize(user)
        for user in Users.query.filter(Users.modified > date).all()
    ]
    if not serialized_labels:
        return "[]"
    return dumps(serialized_labels)


@app.route('/tasks')
def getTasks():
    """
    Get all task records from the database that are newer than the 'modified'
    parameter (or unix epoch if omitted), and return them in a json-encoded
    list
    """
    date = request.args.get('modified',
                            datetime.utcfromtimestamp(0).isoformat(' '))
    serialized_labels = [
        serialize(task, ['deleted'])
        for task in Tasks.query.filter(Tasks.modified > date,
                                       Tasks.deleted == False).all()
    ]
    if not serialized_labels:
        return "[]"
    return dumps(serialized_labels)


@app.route('/deleted/tasks')
def getDeletedTasks():
    """
    Get all task records deleted from the database later than the 'modified'
    parameter (or unix epoch if omitted), and return them in a json-encoded
    list
    """
    date = request.args.get('modified',
                            datetime.utcfromtimestamp(0).isoformat(' '))
    serialized_labels = [
        task._id
        for task in Tasks.query.filter(Tasks.modified > date,
                                       Tasks.deleted == True).all()
    ]
    if not serialized_labels:
        return "[]"
    return dumps(serialized_labels)


@app.route('/add/users', methods=['POST'])
def postUsers():
    """
    Insert a json-encoded list of user records into the database if no user
    exists with that name or
    """
    users = loads(request.data)
    if not users:
        return make_response("[]", 202)

    rejected_users = []
    for user in users:
        if Users.query.filter(Users._id == user["_id"]).first():
            rejected_users.append(user["_id"])
            continue
        new_user = Users(user["name"],
                         user["pwd"],
                         modified=user["modified"],
                         _id=user["_id"])
        db.session.add(new_user)
        try:
            db.session.commit()
        except IntegrityError:
            rejected_users.append(user["_id"])

    if not rejected_users:
        return make_response("[]", 202)
    return make_response(dumps(rejected_users), 202)


@app.route('/add/tasks', methods=['POST'])
def postTasks():
    tasks = loads(request.data)
    if not tasks:
        return make_response("[]", 202)

    rejected_tasks = []
    for task in tasks:
        timestamp = datetime.strptime(task["modified"], "%Y-%m-%d %I:%M:%S")
        db_task = Tasks.query.get(task["_id"])
        if db_task:
            if db_task.modified > timestamp:
                rejected_tasks.append(task["_id"])
            if db_task.modified >= timestamp:
                continue
            db_task.name = task["name"]
            db_task.note = task["note"]
            db_task.checked = task["checked"]
            db_task.noduetime = task["noduetime"]
            db_task.priority = task["priority"]
            db_task.modified = task["modified"]
        else:
            db_task = Tasks(task["name"],
                            task["note"],
                            task["userid"],
                            checked=task["checked"],
                            noduetime=task["noduetime"],
                            duetime=task["duetime"],
                            priority=task["priority"],
                            modified=task["modified"],
                            _id=task["_id"])
        db.session.add(db_task)
        try:
            db.session.commit()
        except IntegrityError:
            rejected_tasks.append(task["_id"])

    if not rejected_tasks:
        return make_response("[]", 202)
    return make_response(dumps(rejected_tasks), 202)


@app.route('/delete/tasks', methods=['POST'])
def deleteTasks():
    tasks = loads(request.data)
    if not tasks:
        return make_response("", 202)

    for task_id in tasks:
        db_task = Tasks.query.filter(Tasks._id == task_id).first()
        if db_task:
            db_task.deleted = True
            db.session.commit()

    return make_response("", 202)


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/login', methods=['POST'])
def login():
    password = hashlib.sha1(request.form['password']).hexdigest()
    username = request.form['username']

    user = Users.query.filter(Users.name == username).first()
    if user is not None and user.pwd == password:
        session['userid'] = user._id
        return redirect(url_for('tasklist'))
    else:
        flash("Invalid username or password.")
        return redirect(url_for('index'))


@app.route('/logout', methods=['POST', 'GET'])
def logout():
    session.pop('userid', None)
    return redirect(url_for('index'))


@app.route('/user', methods=['POST', 'GET'])
def adduser():
    error = None
    if request.method == 'POST':
        username = request.form['username']
        if len(username) > 0:
            if not Users.query.filter(Users.name == username).first():
                password = hashlib.sha1(request.form['password']).hexdigest()
                user = Users(username, password)
                db.session.add(user)
                db.session.commit()
                return redirect(url_for('index'))
            else:
                error = "Invalid username or password."

    return render_template('AddUser.html', error=error)


@app.route('/tasklist')
def tasklist():
    error = None
    user = Users.query.get(session['userid'])
    records = Tasks.query.filter(Tasks.userid == user._id,
                                 Tasks.deleted == False)
    hidden = request.args.get('hide', False)
    if hidden:
        records = records.filter(Tasks.checked == False)
    return render_template('TaskList.html',
                           error=error,
                           name=user.name,
                           records=records.all(),
                           hidden=hidden)


@app.route('/item', methods=['POST', 'GET'])
def additem():
    error = None
    taskid = request.args.get('id', None)
    task = None
    if taskid:
        task = Tasks.query.get(taskid)

    if request.method == 'GET':
            
        if not task:
            task = Tasks('name', '',
                         session['userid'], False, False,
                         date.today().isoformat(), 0)

        return render_template('TaskView.html', error=error, task=task)

    if request.method == 'POST':

        if request.form['submit'] == "Delete Task":
            task.deleted = True
            db.session.add(task)
            db.session.commit()
            return redirect(url_for('tasklist'))
        
        checked = True
        if 'checked' not in request.form:
            checked = False

        duedate = datetime.strptime(request.form["dueDate"], "%Y-%m-%d").date()
        
        if not task:
            task = Tasks(request.form["task"],
                         request.form["note"],
                         session['userid'],
                         checked=checked,
                         noduetime=False,
                         duetime=duedate,
                         priority=request.form["priority"],
                         id=taskid)

        if len(request.form["task"]) == 0:
            error = "Task name cannot be empty."
            return render_template('TaskView.html', error=error, task=task)
        
        task.name = request.form["task"]
        task.note = request.form['note']
        task.duetime = duedate
        task.noduetime = False
        task.checked = checked
        task.priority = request.form['priority']
        db.session.add(task)
        db.session.commit()

        return redirect(url_for('tasklist'))




